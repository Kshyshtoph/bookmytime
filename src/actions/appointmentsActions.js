export const addAppointment = (date, start, end) => ({
  type: "ADD_APPOINTMENT",
  date,
  start,
  end,
});
