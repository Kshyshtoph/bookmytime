export const showBasicModal = (date) => ({
  type: "SHOW_BASIC_MODAL",
  date,
});
export const hideModal = () => ({
  type: "HIDE_MODAL",
});
export const showBookingModal = (hour, isoStartDate, isoEndDate) => ({
  type: "SHOW_BOOKING_MODAL",
  hour,
  isoStartDate,
  isoEndDate,
});
