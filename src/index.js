import React from "react";
import ReactDOM from "react-dom";
import App from "./wiews/App";
import * as serviceWorker from "./serviceWorker";
import { ThemeProvider, createGlobalStyle } from "styled-components";
import { Provider } from "react-redux";
import store from "./store";

const GlobalStyle = createGlobalStyle` 
  *{
    box-sizing: border-box;
  }
  body{
    margin: 0;
    background-color: ${({ theme }) => theme.yellow}
  }
  `;
const theme = {
  primary: "#5bc0eb",
  dark: "#404e4d",
  red: "#c3423f",
  yellow: "#fde74c",
  green: "#9bc53d",
  gray: "#616d6c",
};

ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <ThemeProvider theme={theme}>
        <GlobalStyle />
        <App />
      </ThemeProvider>
    </Provider>
  </React.StrictMode>,
  document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
