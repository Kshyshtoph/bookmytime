import React from "react";
import Day from "./Day";
import styled from "styled-components";

const monthNames = {
  1: "January",
  2: "February",
  3: "March",
  4: "April",
  5: "May",
  6: "June",
  7: "July",
  8: "August",
  9: "September",
  10: "October",
  11: "November",
  12: "December",
};

const generateMonthTable = (monthTable, month, year) => {
  const MonthNumberOfDays = new Date(year, month, 0).getDate();
  const startOfMonth = new Date(year, month, 1).getDay();
  for (let i = 1; i < MonthNumberOfDays; i++) {
    const date = new Date(year, month, i, 12);
    const dayOfMonth = date.getDate();
    const dayOfWeek = date.getDay();

    monthTable.push({
      dayOfWeek,
      dayOfMonth,
      startOfMonth,
      date: date.toISOString().slice(0, 10),
    });
  }
};

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  width: 50%;
  height: 50vh;
  align-items: center;
  padding: 25px;
`;

const GridWrapper = styled.div`
  display: grid;
  width: 100%;
  height: 100%;
  grid-template-columns: repeat(7, 1fr);
  grid-template-rows: repeat(5, 1fr);
`;
const Calendar = ({ month, year }) => {
  const calendarTable = [];
  generateMonthTable(calendarTable, month, year);
  const calendar = calendarTable.map((day) => <Day key={day.date} {...day} />);
  return (
    <Wrapper>
      <h2>{monthNames[month + 1]}</h2>
      <GridWrapper>{calendar}</GridWrapper>
    </Wrapper>
  );
};
export default Calendar;
