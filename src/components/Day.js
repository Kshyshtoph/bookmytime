import React from "react";
import styled from "styled-components";
import { connect } from "react-redux";
import { showBasicModal } from "../actions/modalActions";

const CalendarCard = styled.button`
  grid-row-start: ${({ positionY }) => positionY};
  grid-column: ${({ positionX }) => positionX};
  text-align: center;
  line-height: 50px;
  background-color: ${({ theme }) => theme.primary};
  border: 2px solid ${({ theme }) => theme.dark};
  transition: 0.3s;
  &:hover {
    background-color: ${({ theme }) => theme.green};
    transform: scale(1.1);
  }
`;
const Day = ({ dayOfWeek, dayOfMonth, startOfMonth, date, showModal }) => {
  const positionY = Math.floor((dayOfMonth + startOfMonth - 1) / 7) + 1;
  const handleClick = () => {
    // console.log(date);
    showModal(date);
  };
  return (
    <CalendarCard
      positionY={positionY}
      positionX={dayOfWeek + 1}
      onClick={handleClick}
    >
      {dayOfMonth}
    </CalendarCard>
  );
};

const mapDispatchToProps = (dispatch) => ({
  showModal: (date) => dispatch(showBasicModal(date)),
});
export default connect(null, mapDispatchToProps)(Day);
