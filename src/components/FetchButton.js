import React from "react";
import styled from "styled-components";
import { addAppointment } from "../actions/appointmentsActions";
import { connect } from "react-redux";

const Button = styled.button`
  padding: 20px 40px;
  font-size: 24px;
  background-color: ${({ theme }) => theme.primary};
`;

const FetchButton = ({ addAppointment }) => {
  return (
    <Button
      onClick={() => {}}
    >
      fetch calendar
    </Button>
  );
};

const mapDispatchToProps = (dispatch) => ({
  addAppointment: (date, start, end) =>
    dispatch(addAppointment(date, start, end)),
});
export default connect(null, mapDispatchToProps)(FetchButton);
