import React from "react";
// import styled from "styled-components";
import { hideModal } from "../actions/modalActions";
import { connect } from "react-redux";
// import emailjs from "emailjs-com";
// import { calendarId , templateId userId} from "../config";
import { calendarId } from "../config";
// const templateId = "template_N8uh1IMI";
// const userId = "user_CFr4lJFYx89oCPQ47eTdO";
class BookingModal extends React.Component {
  state = {
    name: "",
    email: "",
    reason: "",
  };
  handleSubmit = (e) => {
    e.preventDefault();
    // emailjs
    //   .send(
    //     "gmail",
    //     templateId,
    //     {
    //       client_name: this.state.name,
    //       appointment_date: this.props.modal.date,
    //       appointment_hour: this.props.modal.hour,
    //       client_email: this.state.email,
    //       issue: this.state.reason,
    //     },
    //     userId
    //   )
    // .then(() => console.log("email succesfully send"))
    // .catch((err) => console.log(err));
    const event = {
      summary: this.state.reason,
      location: "800 Howard St., San Francisco, CA 94103",
      description: `meeting with ${this.state.name} to ${this.state.reason}`,
      start: {
        dateTime: this.props.modal.isoStartDate,
      },
      end: {
        dateTime: this.props.modal.isoEndDate,
      },
    };
    window.gapi.load("client", () => {
      window.gapi.client
        .request({
          path: `https://www.googleapis.com/calendar/v3/calendars/${calendarId}/events`,
          method: "POST",
          body: event,
        })

        .then(() => console.log("event added"))
        .catch((err) => console.log(err));
    });
  };
  handleChange = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  };
  render() {
    console.log(this.props.modal);
    return (
      <>
        <br />
        {this.props.modal.hour}
        <br />
        {this.props.modal.isoDate}
        <br />
        <form onSubmit={this.handleSubmit}>
          <input
            name="name"
            type="text"
            placeholder="your name"
            required
            onChange={this.handleChange}
          />
          <input
            name="email"
            type="email"
            placeholder="your email"
            required
            onChange={this.handleChange}
          />
          <input
            name="reason"
            type="text"
            placeholder="appointment reason"
            required
            onChange={this.handleChange}
          />
          <button type="submit">Send</button>
        </form>
      </>
    );
  }
}

const mapStateToProps = ({ modal }) => ({
  modal,
});

const mapDispatchToProps = (dispatch) => ({
  close: () => dispatch(hideModal()),
});
export default connect(mapStateToProps, mapDispatchToProps)(BookingModal);
