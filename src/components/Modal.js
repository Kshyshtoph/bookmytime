import React from "react";
import styled from "styled-components";
import { connect } from "react-redux";
import { hideModal } from "../actions/modalActions";
import BookingModal from "./BookingModal";
import BasicModal from "./BasicModal";

const CloseBtn = styled.button`
  color: ${({ theme }) => theme.red};
  position: absolute;
  right: 0;
  top: 0;
`;
const Wrapper = styled.div`
  position: absolute;
  left: 50%;
  top: 50%;
  transform: translate(-50%, -50%);
  height: 200px;
  width: 200px;
  background-color: white;
  border: 2px solid ${({ theme }) => theme.red};
`;

const Modal = ({ modal, close }) => {
  return (
    <Wrapper>
      {modal.date}
      {modal.basic ? <BasicModal /> : <BookingModal />}
      <CloseBtn onClick={close}>x</CloseBtn>
    </Wrapper>
  );
};

const mapStateToProps = ({ modal }) => ({
  modal,
});
const mapDispatchToProps = (dispatch) => ({
  close: () => dispatch(hideModal()),
});
export default connect(mapStateToProps, mapDispatchToProps)(Modal);
