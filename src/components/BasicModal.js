import React from "react";
import styled from "styled-components";
import { connect } from "react-redux";
import { showBookingModal } from "../actions/modalActions";

const HourInfo = styled.p`
  text-decoration: none;
  margin: 0;
  padding: 0;
  cursor: pointer;
  color: ${({ taken, theme }) => (taken ? theme.red : theme.dark)};
`;

const BasicModal = ({ appointments, showBookingModal, modal }) => {
  const dayPlan = [
    {
      hour: "10:00-10:55",
      taken: false,
      isoDate: null,
    },
    {
      hour: "11:00-11:55",
      taken: false,
      isoDate: null,
    },
    {
      hour: " 12:00-12:55",
      taken: false,
      isoDate: null,
    },
    {
      hour: "13:00-13:55",
      taken: false,
      isoDate: null,
    },
    {
      hour: "14:00-14:55",
      taken: false,
      isoDate: null,
    },
    {
      hour: "15:00-15:55",
      taken: false,
      isoDate: null,
    },
    {
      hour: "16:00-16:55",
      taken: false,
      isoDate: null,
    },
    {
      hour: "17:00-17:55",
      taken: false,
      isoDate: null,
    },
  ];
  dayPlan.forEach((hour) => {
    const dateArr = modal.date.split("-", 3);
    const startHour = hour.hour.slice(0, 2);
    hour.isoStartDate = new Date(
      dateArr[0],
      dateArr[1] - 1,
      dateArr[2],
      startHour
    ).toISOString();
    hour.isoEndDate = new Date(
      dateArr[0],
      dateArr[1] - 1,
      dateArr[2],
      startHour,
      55
    ).toISOString();
    console.log(hour.isoStartDate, hour.isoEndDate);
    for (let i = 0; i < appointments.length; i++) {
      const isBooked =
        hour.hour.slice(0, 5) === appointments[i].start.slice(11, 16);
      if (isBooked) {
        hour.taken = true;
      }
    }
  });
  const scheduele = dayPlan.map((hour) => (
    <HourInfo
      taken={hour.taken}
      key={hour.hour}
      onClick={() => {
        if (!hour.taken) {
          handleBooking(hour.hour, hour.isoStartDate, hour.isoEndDate);
        }
      }}
    >
      {hour.hour}
    </HourInfo>
  ));
  const handleBooking = (hour, isoStartDate, isoEndDate) => {
    showBookingModal(hour, isoStartDate, isoEndDate);
  };
  return <ul>{scheduele}</ul>;
};

const mapStateToProps = ({ modal, appointments }) => ({
  modal,
  appointments: appointments.filter(
    (appointment) => appointment.date === modal.date
  ),
});
const mapDispatchToProps = (dispatch) => ({
  showBookingModal: (hour, isoStartDate, isoEndDate) =>
    dispatch(showBookingModal(hour, isoStartDate, isoEndDate)),
});
export default connect(mapStateToProps, mapDispatchToProps)(BasicModal);
