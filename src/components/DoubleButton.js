import React from "react";
import ApiCalendar from "react-google-calendar-api";
import styled from "styled-components";

const Button = styled.button`
  padding: 20px 40px;
  font-size: 24px;
  background-color: ${({ theme }) => theme.primary};
`;
class DoubleButton extends React.Component {
  state = {
    isLogged: false,
  };
  handleClick = () => {
    const isLogged = ApiCalendar.isSignedIn;
    this.setState({ isLogged });
    ApiCalendar.handleAuthClick();
  };
  render() {
    return (
      <Button onClick={this.handleClick}>
        {this.state.isLogged ? "Log out" : "Log in"}
      </Button>
    );
  }
}

export default DoubleButton;
