const appointments = (state = [], action) => {
  switch (action.type) {
    default:
      return state;
    case "ADD_APPOINTMENT":
      console.log(action);
      return [
        ...state,
        { start: action.start, end: action.end, date: action.date },
      ];
  }
};
export default appointments;
