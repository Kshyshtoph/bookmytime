import { combineReducers } from "redux";
import appointments from "./appointmentsReducer";
import modal from "./modalReducer";
const rootReducer = combineReducers({ appointments, modal });
export default rootReducer;
