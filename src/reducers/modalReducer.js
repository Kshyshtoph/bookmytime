const modal = (
  state = {
    shown: false,
    date: null,
    basic: true,
    hour: null,
    isoStartDate: null,
    isoEndDate: null,
  },
  action
) => {
  switch (action.type) {
    default:
      return state;
    case "SHOW_BASIC_MODAL":
      return {
        ...state,
        shown: true,
        date: action.date,
        basic: true,
      };
    case "SHOW_BOOKING_MODAL":
      return {
        ...state,
        shown: true,
        basic: false,
        hour: action.hour,
        isoStartDate: action.isoStartDate,
        isoEndDate: action.isoEndDate,
      };
    case "HIDE_MODAL":
      return {
        ...state,
        shown: false,
        date: null,
        hour: null,
        isoStartDate: null,
        isoEndDate: null,
      };
  }
};
export default modal;
