import React from "react";
import { connect } from "react-redux";
import Calendar from "../components/Calendar";
import styled from "styled-components";
import Modal from "../components/Modal";
import { addAppointment } from "../actions/appointmentsActions";
import { calendarId, config } from "../config";

const CalendarWrapper = styled.div`
  display: flex;
`;
const now = new Date();
class App extends React.Component {
  componentDidMount() {
    window.gapi.load("auth2", () => {
      window.gapi.auth2.init(config).then(() => {
        window.gapi.load("client", () => {
          console.log(window.gapi.client);
          window.gapi.client
            .init(config)
            .then(() =>
              window.gapi.client.request({
                path: `https://www.googleapis.com/calendar/v3/calendars/${calendarId}/events`,
              })
            )
            .then(({ result }) =>
              result.items.forEach((item) => {
                this.props.addAppointment(
                  item.start.dateTime.slice(0, 10),
                  item.start.dateTime,
                  item.end.dateTime
                );
              })
            );
        });
      });
    });
  }
  render() {
    return (
      <>
        <CalendarWrapper>
          <Calendar month={now.getMonth()} year={now.getFullYear()} />
          <Calendar
            month={now.getMonth() === 11 ? 1 : now.getMonth() + 1}
            year={
              now.getMonth() === 11 ? now.getFullYear() + 1 : now.getFullYear()
            }
          />
        </CalendarWrapper>
        {this.props.modalShown ? <Modal /> : null}
      </>
    );
  }
}
const mapStateToProps = ({ modal }) => ({
  modalShown: modal.shown,
});

const mapDispatchToProps = (dispatch) => ({
  addAppointment: (date, start, end) =>
    dispatch(addAppointment(date, start, end)),
});
export default connect(mapStateToProps, mapDispatchToProps)(App);
